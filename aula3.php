<!DOCTYPE html>
<html>
    <?php
        /* Declaração de variável
         * Usa-se o '$' para declarar uma variável.
         * As variáveis não são tipadas, com o valor que sera atribuido a ela
         * o interpretador de código indentifica e altera o tipo.
         * 
         * EX.: p1 = p2;
         * Sendo:
         * p1 o nome atribuído à variável
         * p2 o valor atribudo à variável
         */
    
        $nomeCompleto = 'erik Lopes';   // Varriável tipo String
        $int = 10;                      // Variável tipo Int
        $flotOrDouble = 10.2;           // Variável tipo Float ou Double
        $boolean = true;                // Variável tipo Boolean
        
        /* O PHP conta com 8 tipos no total sendo eles:
         * * ESCALARES
         *      ** Int
         *      ** Double ou Float
         *      ** Bool
         *      ** String
         * * COMPOSTO
         *      ** Array
         *      ** Object 
         * * ESPECIAIS
         *      ** Resource
         *      ** NULL
         */
        
        
        /* Declaração de constantes 1
         * Para se declarar uma constante usa-se o método define();
         * 
         * EX.: define(p1,p2); 
         * Sendo:
         * p1 o nome atribuído à constante
         *  **OBS: Deve ser um atributo do tipo String.
         * p2 o valor atribuído à constante
         */
        
        define('constante1', 'PHP');
                
        /* Declaração de constantes 2
         * Outra forma de declarar uma constante é usando o comando const.
         * 
         * EX.: const p1 = p2
         * Sendo:
         * p1 o nome atribuído à constante
         * p2 o valor atribuído à constante
         */
                
        const constante2 = 'mais um pouco de PHP';   
    ?>
    
    <head>
        <meta charset="UTF-8">
        <title> TreinaWeb - Aula 3</title>
    </head>
    <body>
        <?php
            /* Definindo um If
             * 
             * Estrutura:
             * if(Condição){
             *      Se a condição for verdadeira
             * }else{
             *      Se a condição for falsa
             * }
             */
            if($int < 10){ 
                
                /* Echo
                 * 
                 * A instrução echo() escreve o que está dentro dos ()
                 * **OBS: Pode se colocar código HTML no echo que ele irá
                 *  executar como sendo um código HTML mesmo.
                 * 
                 * EX.: echo(p1);
                 * Sendo:
                 * p1 o que deseja imprimir na tela.
                 * **OBS: pode se utilizar uma variável ou contante para
                 *  imprimir o valor da mesma. 
                 */
                
                //Para concatenar se utiliza o ponto final.
                echo('<strong>'. $nomeCompleto . '<strong>');
                
                /*  OBS: para imprimir uma variável deve utilizar
                 *  o $ para convocar seu valor,o mesmo não acontece
                 *  com uma constante.
                 */
            }else{
                echo ('<h1>' . constante1 . '</h1><br>' . constante2);
            }
        ?>
    </body>
</html>