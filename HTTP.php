<html>
    <head>
        <meta charset="UTF-8">
        <title>Primeiro código</title>
    </head>
    <body>
        <!--
        * Action é o script ou págin na qual os dados serão submetidos
        * method é o método de envio dos dados. GET ou POST
        *
        * OBS: GET envia os dados pela url, sendo possível ver o que está sendo
        * enviado. POST envia os dados de forma mais transparente, de uma forma 
        * que somente o navegador e o servidor consegue enxergar.
        -->
        <form action="HTTP2.php" method="post">

            <!-- Definição dos campos do formulário -->
        <!-- Definição dos campos -->
        <fieldset>

            <!-- Campo nome -->
            <p>
                <label for="nome">Nome: </label>
                <input type="text" name="nome" id="nome" />
            </p>

            <!-- Campo email -->
            <p>
                <label for="email">E-mail: </label>
                <input type="text" name="email" id="email" />
            </p>

            <!-- Botão para submeter o formulário -->
            <p><input type="submit" value="Enviar" /></p>
        </fieldset>
        </form>
    </body>
</html>
