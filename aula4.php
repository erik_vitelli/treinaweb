<?php
    //configurando o cabeçalho do html
    header('Content-Type: text/html; charset=utf-8');
    
    //Declaração das variáveis
    $str = 'Erik';
    $bool = true;
    $float = 10.23;
    $int = 123;
    
    function soma($n1, $n2){
        echo $n1 +  $n2;
    }
    
    function formataValor($valor) {
      return  "R$ " . number_format($valor,2,",",".");
    }
    
    echo gettype($str);// Retorna o tipo de dados da variável 
    
    echo '<br>';
    
    echo var_dump($int); // Retorna o tipo e o valor da variável
    
    echo '<br>';
    
    //verificação de tipos
    if (is_bool($bool)){
        echo ' isso é um boolean';
    } else {
        echo 'isso não é um boolean';
    }
    echo '<br>';
     
    
    //funções
    echo soma(50, 20);
    
    echo "<br>";
    
    echo formataValor(1000); // Imprime: R$ 1.000,00

    echo "<br>";

    echo formataValor(2507.30); // Imprime: R$ 2.507,30

?>

