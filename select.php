<?php
    $host = 'localhost';
    $user = 'root';
    $password = '';
    $database = 'db_teste';

    /* Variável de conexão com o database
     * 
     */
    $con = new mysqli($host, $user , $password, $database);

    if ($con->connect_errno){
        exit(utf8_encode($con->error));
    }

    $res = $con->query('select * from cliente');

    if ($con->connect_errno){
        exit(utf8_encode($con->error));
    }
    echo '<table>';
            echo '<tr>';
                echo '<th>ID</th>';
                echo '<th>Nome</th>';
                echo '<th>Email</th>';
                echo '<th>Endereço</th>';
                echo '<th>Telefone</th>';
            echo '</tr>';
            
        while ($cliente = $res->fetch_assoc()){
            echo '<tr>';
                echo '<td>'.$cliente['cliente_id'].'</td>';
                echo '<td>'.$cliente['nome'].'</td>';
                echo '<td>'.$cliente['email'].'</td>';
                echo '<td>'.$cliente['endereco'].'</td>';
                echo '<td>'.$cliente['telefone'].'</td>';
            echo '</tr>';
            
            
        }
    echo '</table>';
?>